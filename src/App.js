import React from 'react'
import './App.css'
import Login from './components/Login/Login'

function App() {
  return (
    <div className="App">
      <header className="app-header">
        <Login />
      </header>
    </div>
  )
}

export default App
