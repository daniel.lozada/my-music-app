const ROUTES = {
  HOME: '/',
  LOGIN: '/log-in',
  SIGNUP: '/sign-up',
  FAVORITES: '/my-favorites',
  REDIRECT: '/redirect',
  ALBUM: '/album',
}

export default ROUTES
