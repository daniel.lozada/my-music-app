import ROUTES from './routes'
import ENDPOINTS from './endpoints'

export { ROUTES, ENDPOINTS }
