import { LoginStyled, LoginButton } from '../Login/loginStyles.js'

const ecopes = ['user-read-currently-playing',
'user-read-recently-played',
'user-read-playback-state',
'user-top-read',
'user-modify-playback-state'];
const redirectUri = 'http://localhost:3000/';
const clientID= 'e23b2d8bbc8c4cb4a0b4fb13f8934c4f';
const endpoint = 'https://accounts.spotify.com/authorize';
const loginURL = `${endpoint}?client_id=${clientID}&response_type=token&redirect_uri=${redirectUri}&scope=${ecopes.join('%20')}&show_dialog=true`;

const Login = () => {
  return (
    <LoginStyled>
      <img src='https://raw.githubusercontent.com/dflr10/dflr10/main/assets/appLogo.png' alt='MyMusicApp logo'/>
      <LoginButton href={loginURL}>Continue with Spotify</LoginButton>
    </LoginStyled>
  )
}

export default Login
