import styled from 'styled-components'

const LoginStyled = styled.div`
  display: grid;
  place-items: center;
  height: 100vh;
  background-color: black;

  & h1 {
    color: white;
  }

  & img {
    width: 100%;
  }
`

const LoginButton = styled.a`
  padding: 1rem 2rem;
  background-color: var(--p-color);
  color: white;
  font-weight: bold;
  text-decoration: none;
  text-align: center;
  border-radius: 30px;
  transition: all 0.3s ease-in-out;

  &:hover {
    box-shadow: 2px 2px 10px 2px rgba(255, 255, 255, 0.2);
  }
`

export { LoginStyled, LoginButton }
